#include<stdio.h>

#define MAX_WORDS 100

main()
{
    int c, nw, i, j, max;

    int isBegin = 0;

    int sums[MAX_WORDS];
    for(i = 0; i < MAX_WORDS; i++)
        sums[i] = 0;

    nw = 0;

    //sum=0;


    while ((c=getchar())!='\n')
    {
        if(c!=' ')
        {
            if(!isBegin) {
                nw++;
                isBegin=1;
            }
            sums[nw-1]++;
        }
        else
        {
            isBegin=0;
        }
    }

    // horizontal diagram
    for(i = 0; i < nw; i++)
    {
        for(j = 0; j < sums[i]; j++)
            printf("*");
        printf("\n");
    }

    // find max word;
    max=0;
    for(i = 0; i < nw; i++)
    {
        if(sums[i] > max)
        {
            max = sums[i];
        }
    }

    printf("max=%d\n", max);

    // vertical diagram:
    for(i = 0; i < max; i++)
    {
        for(j = 0; j < nw; j++)
        {
            if(sums[j] >= max-i)
                printf("*");
            else
                printf(" ");
        }
        printf("\n");
    }

}
