#include <stdio.h>

void change(int* p);

main()
{
    int x;
    x=0;
   printf("inside main x=%d\n", x);
   int* px;
   px = &x;
   change(px);
   printf("inside main x=%d\n", x);
}

 void change(int* p)
 {
    *p = *p + 1;
    printf("inside change x=%d\n", *p);
 }
