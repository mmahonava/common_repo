#include<stdio.h>

// Define constants:
#define MAX_WORDS 1024
#define WORD_IN 1
#define WORD_OUT 0

main()
{
    //
    //  Declare varibales:
    //
    int c,
        nw, // количесвто слов
        i, j, max,
        isBegin, // флажок для отмечания в слове мы сейчас или нет
        letterQuantitis[MAX_WORDS]; // массив для подсчёта количества букв в каждом слове

    //
    // Initialization:
    //
    isBegin = WORD_OUT;

    // инициализируем массив нулями:
    for(i = 0; i < MAX_WORDS; i++)
        letterQuantitis[i] = 0;

    nw = 0;

    //
    // LOGICS
    //

    // Посимовльный ввод:
    while ((c=getchar())!='\n') // пока не конец строки:
    {
        if(c!=' ') // если c - это символ:
        {
            if(isBegin == WORD_OUT) // если не было начала, то значит началось новое слово...
            {
                isBegin=WORD_IN; // следовательно, слово началось
                nw++; // увеличиваем счетчик слов
            }

            letterQuantitis[nw-1]++;
        }
        else // если с - это пробел:
        {
            isBegin=WORD_OUT;
        }
    }


    max=0;
    for(i=0; i< nw; i++)
        if (max<letterQuantitis[i])
            max=letterQuantitis[i];
    printf("max=%d\n", max);

    int M[nw][max];
    for(i=0; i<nw; i++)
        for(j=0; j<max; j++)
            M[i][j] = 0;

    for(i=0; i<nw; i++)
    {
        for(j=0; j<max; j++)
            printf("%d ", M[i][j]);
        printf("\n");
    }

    for(i=0;i<nw;i++)
    {
        for(j=0;j<max;j++)
        {
            if (j<letterQuantitis[i])
            {
                M[i][j]=1;
            }
            else
                M[i][j]=0;
        }
     }
    printf("horizontal histogram is\n");
     for(i=0; i<nw; i++)
    {
        for(j=0; j<max; j++)
            printf("%d ", M[i][j]);
        printf("\n");
    }

    printf("vertical dig\n");
    for(i=max-1;i>=0;i--)
    {
        for(j=0;j<nw;j++)
            printf("%d ", M[j][i]);
        printf("\n");
    }

//    int M2[max][nw];
//        for (i=0; i<nw; i++)
//            {
//            for(j=0; j<max; j++)
//                {
//                    M2[max-j-1][i]=M[i][j];
//                }
//            }
//    printf("vertical histogram is\n");
//    for(i=0; i<max; i++)
//    {
//        for(j=0; j<nw; j++)
//            printf("%d ", M2[i][j]);
//        printf("\n");
//    }

//    // horizontal diagram
//    for(i = 0; i < nw; i++)
//    {
//        for(j = 0; j < letterQuantitis[i]; j++)
//            printf("*");
//        printf("\n");
//    }
//
//    // find max word;
//    max=0;
//    for(i = 0; i < nw; i++)
//    {
//        if(letterQuantitis[i] > max)
//        {
//            max = letterQuantitis[i];
//        }
//    }
//
//    printf("max=%d\n", max);
//
//    // vertical diagram:
//    for(i = 0; i < max; i++)
//    {
//        for(j = 0; j < nw; j++)
//        {
//            if(letterQuantitis[j] >= max-i)
//                printf("*");
//            else
//                printf(" ");
//        }
//        printf("\n");
//    }

}
