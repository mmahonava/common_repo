#include <stdio.h>

#define true 1
#define false 0

#define NAME_MAX_LENGTH 10
#define GOODS_MAX_AMOUNT 15

#define MENU_OUTPUT 1
#define MENU_CREATE_RECORD 2
#define MENU_DELETE_RECORD 3
#define MENU_EXIT 0

struct Good
{
    char name[NAME_MAX_LENGTH];
    int amount;
    double price;
};

void cleanStdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

void pause()
{
    printf("Enter any key to continue . . . ");
    cleanStdin();
    getchar();
}

int chooseAction()
{
    int menuItem;
    printf("_____________________________________________\n");
    printf("Welcome to Console program!\n");
    printf("Please, choose one action below:\n");
    printf("1. Show all records.\n");
    printf("2. Input new record.\n");
    printf("3. Remove record.\n");
    printf("0. Exit.\n");
    printf("Enter menu item's number: ");

    scanf("%d", &menuItem);
    return menuItem;
}

void outputRecords(int goodsAmount, struct Good goods[])
{
    int i;
    printf("All records:\n");
    printf("Name       Amount     Price,$   \n");
    for(i = 0; i < goodsAmount; i++)
    {
        printf("%10s %10d %10.1f\n", goods[i].name, goods[i].amount, goods[i].price);
    }
    pause();
}

void createRecord(int *pGoodsAmount, struct Good goods[])
{
    printf("Input of new record.\n");
    if(*pGoodsAmount < GOODS_MAX_AMOUNT)
    {
        printf("enter name: ");
        scanf("%s", goods[*pGoodsAmount].name);
        printf("Enter amount: ");
        scanf("%d", &goods[*pGoodsAmount].amount);
        printf("Enter price: ");
        scanf("%lf", &goods[*pGoodsAmount].price);
        (*pGoodsAmount)++;
    }
    else
    {
        printf("Can not add new record because memory is full.\n");
    }
    pause();
}

void deieteRecord(int *pGoodsAmount, struct Good goods[])
{
    int delI, i;

    printf("Remove record.\n");
    printf("please, enter a number of removing record: ");
    scanf("%d", &delI);
    delI--;
    if(delI>=0 && delI<*pGoodsAmount)
    {
        for(i=delI; i<*pGoodsAmount-1; i++)
        {
        goods[i]=goods[i+1];
        }
        (*pGoodsAmount)--;
    }
    else
    {
        printf("You entered wrong.\n");
    }
    pause();
    }

main()
{
    int menuItem, isExit=false, goodsAmount=0;
    struct Good goods[GOODS_MAX_AMOUNT];

    while(!isExit)
    {
        menuItem = chooseAction();

        switch(menuItem)
        {
            case MENU_OUTPUT:
            {
                outputRecords(goodsAmount, goods);
                break;
            }
            case MENU_CREATE_RECORD:
            {
                createRecord(&goodsAmount, goods);
                break;
            }
            case MENU_DELETE_RECORD:
            {
                deieteRecord(&goodsAmount, goods);
                break;
            }
            case MENU_EXIT:
            printf("Exiting...\n");
            isExit=true;
            break;
            default:
            printf("You entered non-existing menu item. Please, try again . . . \n");
            pause();
            break;
        }


    }
}
