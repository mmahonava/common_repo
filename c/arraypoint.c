#include <stdio.h>

#define n 3

void changeAarray(double* p)
{
   int i;
    for (i=0; i<n; i++)
        *(p+i)=*(p+i)+1;
}
void printArray(double A[])
{
    int i;
    for (i=0; i<n; i++)
    {
        printf("A[%d]=%f\n", i, A[i]);
    }
}
main()
{

    int i;
    double A[n];

    for (i=0; i<n; i++)
        A[i]=i;

    printArray(A);
    changeAarray(A);
    printArray(A);
}



