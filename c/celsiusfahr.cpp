#include<stdio.h>

main()
{
	float celsius,fahr;
	int lower, upper, step;
	
	lower=0;
	upper=300;
	step=20;
	
	printf("celsius\tfahr\n");
	celsius=lower;
	while(celsius<=upper){
		fahr=(9/5)*celsius+32;
		printf("%6.0f%5.0f\n",celsius,fahr);
		celsius=celsius+step;
	}
}
