
public class Bitwise {
	public static void main(String[] args){
		int x1=0x5;
		int x2=0xa;
		System.out.println("x1=" + Integer.toBinaryString(x1));
		System.out.println("x2=" + Integer.toBinaryString(x2));
		System.out.println("~x1=" + Integer.toBinaryString(~x1));
		System.out.println("~x2=" + Integer.toBinaryString(~x2));
		System.out.println("x1&x2=" + Integer.toBinaryString(x1&x2));
		System.out.println("x1|x2=" + Integer.toBinaryString(x1|x2));
		System.out.println("x1^x2=" + Integer.toBinaryString(x1^x2));
	}

}
