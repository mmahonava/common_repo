import java.util.Random;

public class Bool {
	public static void main(String[] args){
		Random rand=new Random();
		boolean flip = rand.nextBoolean();
		System.out.println("outcome: ");
		System.out.println(flip ? "head" : "tail");
		
	}

}
