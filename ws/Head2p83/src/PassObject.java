
class Letter{
	int c;
	float t;
}
public class PassObject {
	static void f(Letter y){
		y.c='z';
	}
	static void f1(Letter g){
		g.t='a'*2;
	}
	static void f2(Letter m){
		m.t='b'%5;
	}
public static void main(String[] args){
	Letter x= new Letter();
	x.c='a';
	System.out.println("1: x.c:" + x.c);
	f1(x);
	System.out.println("1a: x.t:" + x.t);
	f(x);
	System.out.println("2: x.c:" + x.c);
	f2(x);
	System.out.println("3: x.t:" + x.t);
}
}

