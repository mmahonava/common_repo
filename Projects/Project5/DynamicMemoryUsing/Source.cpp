#include<iostream>
#include<Windows.h>

using namespace std;

int main()
{
	int m;
	cout << "enter amount of numbers: ";
	cin >> m;

	int* NUMBERS = new int[m];

	for (int i = 0; i < m; i++)
	{
		cout << "enter i-element: ";
		cin >> NUMBERS[i];
	}

	int sum = 0;
	for (int i = 0; i < m; i++)
	{
		sum = sum + NUMBERS[i];
	}
	cout << "sum: " << sum << endl;

	double avg = (double)sum / m;
	cout << "avg: " << avg<<endl;

	delete[] NUMBERS;

	system("pause");
	return 0;
}