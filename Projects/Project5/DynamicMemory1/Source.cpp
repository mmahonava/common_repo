#include <stdio.h>
#include <malloc.h>

#define MAX 10

int main()
{

	int array[MAX]; // static array
	int x;//static variable

	// DYNAMIC MEMORY

	// using approuch in C
	//double* pd;
	//pd = (double*)alloca(sizeof(double));//allocation memory
	//*pd = 1;
	//printf_s("pd=%f", *pd);
	//free((void*)pd);//cleaning of memory

	//using C++ approach
	double* pd2=new double; // declaration of variable
	*pd2 = 5;
	printf_s("pd2=%f", *pd2);
	delete pd2; // delete var
	//printf_s("pd2=%f", *pd2); - this does not work here

	int* A = new int[MAX];// declare array

	delete[] A;//delete array


	getchar();
	return 0;
}