﻿#include <stdio.h>

// функция с использованием указателя
//void incr(int* pi)
//{
//	(*pi) = (*pi) + 10;
//	return;
//}

// функция с использованием ссылки
void incr(int& pi)
{
	pi = pi + 10;
	return;
}



int main()
{
	printf("hello\n");
	
    int	i = 1;
	incr(i);
	printf_s("%d", i);

	getchar();
	return 0;
}