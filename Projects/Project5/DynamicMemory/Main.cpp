#include <stdio.h>
#include <malloc.h>

#define MAX 10

int main()
{

	int array[MAX]; // static array
	int x;//static variable

	//DYNAMIC MEMORY

	// using approuch in C
	double* pd;
	pd = (double*) alloca(sizeof(double));//allocation memory
	*pd = 1;
	printf_s("pd=%f", *pd);
	free((void*)pd);//cleaning of memory
	
	return 0;
}